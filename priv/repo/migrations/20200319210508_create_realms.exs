defmodule Router.Auth.Repo.Migrations.CreateRealms do
  use Ecto.Migration

  def change do
    create table("tenants", primary_key: false) do
      add(:id, :binary_id, primary_key: true, default: fragment("gen_random_uuid()"))
      add(:uri, :string, null: false)
      timestamps()
    end

    create(unique_index(:tenants, [:uri]))

    create table("realms", primary_key: false) do
      add(:id, :binary_id, primary_key: true, default: fragment("gen_random_uuid()"))
      add(:uri, :string, null: false)
      add(:tenant_id, references(:tenants, type: :binary_id, on_delete: :delete_all), null: false)
      timestamps()
    end

    create(unique_index(:realms, [:uri]))

    create table("roles", primary_key: false) do
      add(:id, :binary_id, primary_key: true, default: fragment("gen_random_uuid()"))
      add(:name, :string, null: false)
      add(:super, :boolean, null: false, default: false)
      add(:tenant_id, references(:tenants, type: :binary_id, on_delete: :delete_all))
      timestamps()
    end

    create(unique_index(:roles, [:name, :tenant_id]))

    create table("permissions", primary_key: false) do
      add(:id, :binary_id, primary_key: true, default: fragment("gen_random_uuid()"))
      add(:type, :string, null: false)
      add(:rule, :string, null: false)
      add(:role_id, references(:roles, type: :binary_id, on_delete: :delete_all))
      timestamps()
    end

    create table("peers", primary_key: false) do
      add(:id, :binary_id, primary_key: true, default: fragment("gen_random_uuid()"))
      add(:authid, :string, null: false)
      add(:password, :string, null: false)
      add(:salt, :string, null: false)
      add(:iterations, :integer, null: false)
      add(:keylen, :integer, null: false)
      add(:cidr, :string)
      add(:realm_id, references(:realms, type: :binary_id, on_delete: :delete_all), null: false)
      add(:tenant_id, references(:tenants, type: :binary_id, on_delete: :delete_all), null: false)
      timestamps()
    end

    create(index(:peers, [:authid]))
    create(unique_index(:peers, [:authid, :realm_id]))

    create table("peers_roles", primary_key: false) do
      add(:peer_id, references(:peers, type: :binary_id, on_delete: :delete_all))
      add(:role_id, references(:roles, type: :binary_id, on_delete: :delete_all))
    end

    create(index(:peers_roles, [:peer_id]))
    create(index(:peers_roles, [:role_id]))
    create(unique_index(:peers_roles, [:peer_id, :role_id]))
  end
end
