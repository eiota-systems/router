FROM hexpm/elixir:1.12.3-erlang-24.0.6-alpine-3.14.0 as build

# # install build dependencies
RUN apk add --no-cache \
  gcc \
  g++ \
  git \
  make \
  musl-dev \
  linux-headers
RUN mix do local.hex --force, local.rebar --force
WORKDIR /app

FROM build as deps

COPY mix.exs mix.lock ./

ARG MIX_ENV=prod
RUN mix deps.get --only=$MIX_ENV
RUN mix deps.compile

FROM deps as releaser
COPY . .
ENV MIX_ENV=prod
RUN mix release app

FROM alpine:3.14
RUN apk add --no-cache bash libstdc++ openssl musl-dev
WORKDIR /app
COPY --from=releaser /app/_build/prod/rel/app ./

EXPOSE 4000

ENTRYPOINT ["/app/bin/app"]
