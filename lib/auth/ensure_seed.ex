defmodule Router.Auth.EnsureSeed do
  @moduledoc false
  require Logger
  use GenServer

  alias Router.Auth.{Peer, Permission, Realm, Repo, Role, Tenant}
  @ecto_repos [Repo]

  def start_link(tenant: tenant, uri: uri, authid: authid, password: password) do
    GenServer.start_link(__MODULE__, {uri, authid, password, tenant})
  end

  def init({uri, authid, password, tenant}) do
    ensure_migrations()
    %Tenant{} = tenant = Tenant.create(uri: tenant)
    %Realm{} = realm = Realm.create(uri: uri, tenant: tenant)
    %Peer{} = peer = Peer.create(authid: authid, password: password, realm: realm, tenant: tenant)
    %Role{} = super_role = Role.create(name: "super", tenant: tenant, super: true)
    maybe_add_permission(super_role, "*", "*")
    %Role{} = admin_role = Role.create(name: "admin", tenant: tenant, super: false)
    maybe_add_permission(admin_role, "*", "admin.")
    maybe_add_super_role(peer, super_role)
    :ignore
  end

  defp maybe_add_super_role(%Peer{roles: []} = peer, role) do
    Peer.add_roles(peer, [role])
  end

  defp maybe_add_super_role(peer, _role), do: peer

  defp maybe_add_permission(%Role{permissions: perms} = role, type, rule) do
    case Enum.find(perms, fn
           %Permission{type: ^type, rule: ^rule} -> true
           _ -> false
         end) do
      nil ->
        Permission.create(type: type, rule: rule, role: role)

      _ ->
        :noop
    end
  end

  defp ensure_migrations do
    Logger.info("Ensuring tables have been migrated")

    for repo <- @ecto_repos do
      {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :up, all: true))
    end
  end
end
