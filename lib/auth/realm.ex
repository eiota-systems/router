defmodule Router.Auth.Realm do
  @moduledoc """
  CREATE TABLE authentication.realms (
        id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
        uri STRING(255) NOT NULL UNIQUE,
        tenant_id UUID NOT NULL REFERENCES authentication.realms (id) ON DELETE CASCADE,
        inserted_at TIMESTAMP NOT NULL,
        updated_at TIMESTAMP NOT NULL
  );

  """
  use Ecto.Schema

  alias __MODULE__
  alias Router.Auth.{Peer, Repo, Tenant}

  @primary_key {:id, :binary_id, autogenerate: false, read_after_writes: true}
  @foreign_key_type :binary_id
  schema "realms" do
    field(:uri, :string)
    belongs_to(:tenant, Tenant)
    has_many(:peers, Peer)
    timestamps()
  end

  def get_all do
    Realm
    |> Repo.all()
    |> Repo.preload([[peers: :roles]])
  end

  def get(uri: uri) do
    Realm
    |> Repo.get_by(uri: uri)
    |> Repo.preload([[peers: :roles], :tenant])
  end

  def create(uri: uri, tenant: tenant) do
    {:ok, r} =
      %Realm{uri: uri, tenant: tenant}
      |> Repo.insert()
      |> Repo.preload([[peers: :roles], :tenant])

    r
  rescue
    _er -> get(uri: uri)
  end

  def to_map(realms) when is_list(realms) do
    Enum.map(realms, &to_map/1)
  end

  def to_map(
        %Realm{
          id: id,
          peers: peers,
          uri: uri,
          updated_at: ua,
          inserted_at: ia
        } = realm
      ) do
    %Realm{tenant: %Tenant{uri: tenant_uri}} = Repo.preload(realm, [:tenant])

    %{
      id: id,
      uri: uri,
      tenant: tenant_uri,
      peers: Peer.to_map(peers),
      updated_at: NaiveDateTime.to_iso8601(ua),
      inserted_at: NaiveDateTime.to_iso8601(ia)
    }
  end

  def to_map(_), do: :not_loaded
end
