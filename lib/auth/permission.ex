defmodule Router.Auth.Permission do
  @moduledoc """
  """

  use Ecto.Schema
  alias __MODULE__
  alias Router.Auth.{Repo, Role}

  @primary_key {:id, :binary_id, autogenerate: false, read_after_writes: true}
  @foreign_key_type :binary_id
  schema "permissions" do
    field(:type, :string)
    field(:rule, :string)
    belongs_to(:role, Role)
    timestamps()
  end

  def get(id: id) do
    Repo.get(Permission, id)
  end

  def create(type: type, rule: rule, role: role) do
    {:ok, u} =
      %Permission{type: type, rule: rule, role: role}
      |> Repo.insert()

    u
  end

  def to_map(perms) when is_list(perms) do
    Enum.map(perms, &to_map/1)
  end

  def to_map(%Permission{type: type, rule: rule}) do
    %{
      type: type,
      rule: rule
    }
  end

  def to_map(_), do: :not_loaded
end
