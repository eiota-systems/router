defmodule Router.Auth.Peer do
  @moduledoc """
  CREATE TABLE authentication.peers (
        id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
        authid STRING(255) NOT NULL,
        password STRING NOT NULL,
        salt STRING NOT NULL,
        iterations INT NOT NULL,
        keylen INT NOT NULL,
        cidr STRING,
        realm_id UUID NOT NULL REFERENCES authentication.realms (id) ON DELETE CASCADE,
        inserted_at TIMESTAMP NOT NULL,
        updated_at TIMESTAMP NOT NULL,
        UNIQUE (authid, realm_id),
        INDEX (authid)
  );

  """

  use Ecto.Schema
  require Ecto.Query
  alias __MODULE__
  alias Router.Auth.{Realm, Repo, Role, Tenant}
  alias Wampex.Crypto

  @primary_key {:id, :binary_id, autogenerate: false, read_after_writes: true}
  @foreign_key_type :binary_id
  schema "peers" do
    field(:authid, :string)
    field(:password, :string)
    field(:salt, :string)
    field(:iterations, :integer)
    field(:keylen, :integer)
    field(:cidr, :string)
    belongs_to(:realm, Realm)
    belongs_to(:tenant, Tenant)
    many_to_many(:roles, Role, join_through: "peers_roles")
    timestamps()
  end

  def get_for_tenant(tenant: %Tenant{id: id}) do
    Peer
    |> Ecto.Query.where(tenant_id: ^id)
    |> Repo.all()
    |> Repo.preload(roles: :permissions)
  end

  def get_for_realm(realm: %Realm{id: id}) do
    Peer
    |> Ecto.Query.where(realm_id: ^id)
    |> Repo.all()
    |> Repo.preload(roles: :permissions)
  end

  def get(authid: _authid, realm: nil) do
    :invalid_realm
  end

  def get(authid: authid, realm: realm) do
    Peer
    |> Repo.get_by(authid: authid, realm_id: realm.id)
    |> Repo.preload([[roles: :permissions], :tenant])
  end

  def add_roles(%Peer{} = peer, roles) do
    peer
    |> Repo.preload(:roles)
    |> Ecto.Changeset.change()
    |> Ecto.Changeset.put_assoc(:roles, roles)
    |> Repo.update!()
  end

  def create(authid: authid, password: password, realm: realm, tenant: tenant) do
    keylen = Application.get_env(:wampex_router, :keylen)
    salt = Crypto.random_string(keylen)
    iterations = Enum.random(10_000..50_000)

    password = Crypto.pbkdf2(password, salt, iterations, keylen)

    {:ok, u} =
      %Peer{
        authid: authid,
        password: password,
        realm: realm,
        tenant: tenant,
        salt: salt,
        iterations: iterations,
        keylen: keylen
      }
      |> Repo.insert()
      |> Repo.preload(roles: :permissions)

    u
  rescue
    _er ->
      get(authid: authid, realm: realm)
  end

  def to_map(peers) when is_list(peers) do
    Enum.map(peers, &to_map/1)
  end

  def to_map(%Peer{id: id, authid: authid, roles: roles}) do
    %{
      id: id,
      authid: authid,
      roles: Role.to_map(roles)
    }
  end

  def to_map(_), do: :not_loaded
end
