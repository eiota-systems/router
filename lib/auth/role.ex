defmodule Router.Auth.Role do
  @moduledoc """
  """

  use Ecto.Schema
  require Ecto.Query
  alias __MODULE__
  alias Router.Auth.{Peer, Permission, Repo, Tenant}

  @primary_key {:id, :binary_id, autogenerate: false, read_after_writes: true}
  @foreign_key_type :binary_id
  schema "roles" do
    field(:name, :string)
    field(:super, :boolean)
    belongs_to(:tenant, Tenant)
    has_many(:permissions, Permission)
    many_to_many(:peers, Peer, join_through: "peers_roles")
    timestamps()
  end

  def get_for_tenant(tenant: %Tenant{id: id}) do
    Role
    |> Ecto.Query.where(tenant_id: ^id)
    |> Repo.all()
    |> Repo.preload([:permissions])
  end

  def get(name: name, tenant: tenant) do
    Role
    |> Repo.get_by(name: name, tenant_id: tenant.id)
    |> Repo.preload([:permissions])
  end

  def get(id: id) do
    Role
    |> Repo.get(id)
    |> Repo.preload([:permissions])
  end

  def create(name: name, tenant: tenant, super: super) do
    {:ok, u} =
      %Role{name: name, tenant: tenant, super: super}
      |> Repo.insert()
      |> Repo.preload([:permissions])

    u
  rescue
    _er -> get(name: name, tenant: tenant)
  end

  def to_map(roles) when is_list(roles) do
    Enum.map(roles, &to_map/1)
  end

  def to_map(%Role{id: id, name: name, super: supa, permissions: perms}) do
    %{
      id: id,
      name: name,
      super: supa,
      permissions: Permission.to_map(perms)
    }
  end

  def to_map(_), do: :not_loaded
end
