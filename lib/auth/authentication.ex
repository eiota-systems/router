defmodule Router.Auth.Authentication do
  @moduledoc false
  @behaviour Wampex.Router.Authentication
  require Logger

  alias Router.Auth.{Peer, Realm, Role}
  alias Wampex.Crypto
  alias Wampex.Serializers.JSON

  @wampcra "wampcra"
  @auth_provider "router"
  @super "super"
  @admin "admin"
  @peer "peer"

  @impl true
  def authenticate?(methods) do
    can_auth(methods)
  end

  @impl true
  def method, do: @wampcra

  @impl true
  def challenge(realm, authid, session_id) do
    %Realm{} = realm = Realm.get(uri: realm)
    %Peer{} = peer = Peer.get(authid: authid, realm: realm)
    role = get_role(peer.roles, @peer)
    now = DateTime.to_iso8601(DateTime.utc_now())

    %{
      challenge:
        JSON.serialize!(%{
          nonce: Crypto.random_string(peer.keylen),
          authprovider: @auth_provider,
          authid: authid,
          timestamp: now,
          authrole: role,
          authmethod: @wampcra,
          session: session_id
        }),
      salt: peer.salt,
      keylen: peer.keylen,
      iterations: peer.iterations
    }
  end

  @impl true
  def parse_challenge(challenge) do
    ch = JSON.deserialize!(challenge.challenge)

    {get_in(ch, ["authid"]), get_in(ch, ["authrole"]), get_in(ch, ["authmethod"]),
     get_in(ch, ["authprovider"])}
  end

  @impl true
  def authenticate(signature, realm, authid, %{
        challenge: challenge
      }) do
    %Peer{password: password} = peer = get_peer(authid, realm)

    {password
     |> Crypto.hash_challenge(challenge)
     |> Crypto.compare_secure(signature), peer}
  end

  defp get_peer(authid, uri) do
    realm = Realm.get(uri: uri)
    Peer.get(authid: authid, realm: realm)
  end

  defp get_role([], last), do: last
  defp get_role([%Role{super: true} | _], _), do: @super
  defp get_role([%Role{name: @admin} | t], _), do: get_role(t, @admin)
  defp get_role([_ | t], last), do: get_role(t, last)

  defp can_auth([]), do: false
  defp can_auth([@wampcra | _]), do: true
  defp can_auth([_ | t]), do: can_auth(t)
end
