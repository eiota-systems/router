defmodule Router.Auth.Authorization do
  @moduledoc false
  @behaviour Wampex.Router.Authorization

  alias Router.Auth.{Peer, Permission, Role}

  @impl true
  def authorized?(_type, "admin.get_tenants", %Peer{roles: roles}) do
    Enum.any?(roles, fn
      %Role{super: true} -> true
      _ -> false
    end)
  end

  @impl true
  def authorized?(_type, "admin.create_tenant", %Peer{roles: roles}) do
    Enum.any?(roles, fn
      %Role{super: true} -> true
      _ -> false
    end)
  end

  @impl true
  def authorized?(type, uri, %Peer{roles: roles}) do
    ta = Atom.to_string(type)

    Enum.any?(roles, fn %Role{permissions: p} ->
      Enum.any?(p, fn
        %Permission{type: "*", rule: "*"} -> true
        %Permission{type: ^ta, rule: r} -> String.starts_with?(uri, r)
        _ -> false
      end)
    end)
  end
end
