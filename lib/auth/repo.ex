defmodule Router.Auth.Repo do
  use Ecto.Repo,
    otp_app: :router,
    adapter: Ecto.Adapters.Postgres

  alias __MODULE__
  require Logger

  def init(:runtime, config) do
    {:ok, get_config(config)}
  end

  def init(:supervisor, config) do
    config = get_config(config)
    ensure_db_created(config)
    {:ok, config}
  end

  defp get_config(config) do
    config
    |> update_config(:database, "AUTH_DATABASE_NAME", nil)
    |> update_config(:hostname, "AUTH_DATABASE_HOSTAME", nil)
    |> update_config(:port, "AUTH_DATABASE_PORT", &String.to_integer/1)
    |> update_config(:username, "AUTH_DATABASE_USERNAME", nil)
    |> update_config(:password, "AUTH_DATABASE_PASSWORD", nil)
  end

  defp ensure_db_created(config) do
    Logger.info("Ensuring DB exists")
    r = Repo.__adapter__().storage_up(config)
    Logger.info("DB Creation: #{inspect(r)}")
  end

  defp update_config(config, key, env, format) do
    case System.get_env(env) do
      nil ->
        config

      val ->
        Keyword.put(config, key, do_format(val, format))
    end
  end

  defp do_format(val, nil), do: val
  defp do_format(val, fmt), do: fmt.(val)
end
