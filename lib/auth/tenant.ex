defmodule Router.Auth.Tenant do
  @moduledoc """
  CREATE TABLE auth.tenant (
        id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
        uri STRING(255) NOT NULL UNIQUE,
        inserted_at TIMESTAMP NOT NULL,
        updated_at TIMESTAMP NOT NULL
  );

  """
  use Ecto.Schema

  alias __MODULE__
  alias Router.Auth.{Peer, Realm, Repo, Role}

  @primary_key {:id, :binary_id, autogenerate: false, read_after_writes: true}
  @foreign_key_type :binary_id
  schema "tenants" do
    field(:uri, :string)
    has_many(:realms, Realm)
    has_many(:peers, Peer)
    has_many(:roles, Role)
    timestamps()
  end

  def get_all do
    Tenant
    |> Repo.all()
    |> Repo.preload([:realms, :peers, [roles: :permissions]])
  end

  def get(uri: uri) do
    Tenant
    |> Repo.get_by(uri: uri)
    |> Repo.preload([:realms, :peers, [roles: :permissions]])
  end

  def create(uri: uri) do
    {:ok, r} =
      %Tenant{uri: uri}
      |> Repo.insert()
      |> Repo.preload([:realms, :peers, [roles: :permissions]])

    r
  rescue
    _er -> get(uri: uri)
  end

  def to_map(tenants) when is_list(tenants) do
    Enum.map(tenants, &to_map/1)
  end

  def to_map(%Tenant{
        id: id,
        uri: uri,
        roles: roles,
        realms: realms,
        updated_at: ua,
        inserted_at: ia
      }) do
    %{
      id: id,
      uri: uri,
      realms: Realm.to_map(realms),
      roles: Role.to_map(roles),
      updated_at: NaiveDateTime.to_iso8601(ua),
      inserted_at: NaiveDateTime.to_iso8601(ia)
    }
  end

  def to_map(_), do: :not_loaded
end
