defmodule Router do
  @moduledoc """
  Documentation for Router.
  """
  use Application

  require Logger

  alias Router.Admin
  alias Router.Auth.{Authentication, Authorization, EnsureSeed, Repo}
  alias Wampex.Client
  alias Wampex.Client.Session
  alias Wampex.Client.Authentication, as: ClientAuthentication
  alias Wampex.Client.Realm
  alias Wampex.Crypto
  alias Wampex.Roles.Publisher
  alias Wampex.Router, as: WampexRouter

  def start(_type, _opts) do
    topologies = get_sys_arg("DEPLOYMENT", "local") |> topology()
    tenant = get_sys_arg("SEED_TENANT", "org.entropealabs")
    realm = get_sys_arg("SEED_REALM", "admin")
    authid = get_sys_arg("SEED_AUTHID", "admin")
    password = get_sys_arg("SEED_PASSWORD", Crypto.random_string(32))
    platform_password = get_sys_arg("PLATFORM_PASSWORD", Crypto.random_string(32))
    name = Router
    client_url = "ws://localhost:4000/ws"

    client_authentication = %ClientAuthentication{
      authid: authid,
      authmethods: ["wampcra"],
      secret: password
    }

    client_realm = %Realm{name: realm, authentication: client_authentication}

    client_roles = [Publisher]
    client_session = %Session{url: client_url, realm: client_realm, roles: client_roles}

    children = [
      Repo,
      {EnsureSeed, [tenant: tenant, uri: realm, authid: authid, password: password]},
      {WampexRouter,
       [
         name: name,
         port: 4000,
         topologies: topologies,
         replicas: 3,
         quorum: 1,
         authentication_module: Authentication,
         authorization_module: Authorization
       ]},
      {Client, name: AdminClient, session: client_session, reconnect: true},
      {Admin,
       name: admin_name(name),
       db: WampexRouter.db_name(name),
       realm: realm,
       realms: WampexRouter.realms_name(name),
       platform_password: platform_password}
    ]

    Supervisor.start_link(children, strategy: :one_for_one, name: Router.Supervisor)
  end

  defp admin_name(name), do: Module.concat(name, Admin)

  defp topology("kubernetes"), do: []

  defp topology(_) do
    [
      kv: [
        strategy: Cluster.Strategy.Epmd,
        config: [
          hosts: [
            :"app@router1.eiota.systems",
            :"app@router2.eiota.systems",
            :"app@router3.eiota.systems"
          ]
        ],
        connect: {:net_kernel, :connect_node, []},
        disconnect: {:erlang, :disconnect_node, []},
        list_nodes: {:erlang, :nodes, [:connected]}
      ]
    ]
  end

  defp topology()

  defp get_sys_arg(key, default) do
    case System.get_env(key) do
      nil ->
        Logger.warn("#{key} not available in environment, using: #{default}")
        default

      val ->
        val
    end
  end
end
