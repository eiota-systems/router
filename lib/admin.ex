defmodule Router.Admin do
  @moduledoc false
  use GenServer
  require Logger

  alias Router.Auth.{Peer, Permission, Realm, Role, Tenant}
  alias Wampex.Client
  alias Wampex.Roles.Dealer.{Invocation, Result}
  alias Wampex.Roles.Peer.Error
  alias Wampex.Roles.Publisher.Publish
  alias Wampex.Router.Realms
  alias Wampex.Router.Realms.Session, as: RealmSession

  @procedures [
    "admin.create_tenant",
    "admin.create_realm",
    "admin.create_peer",
    "admin.create_role",
    "admin.get_tenants",
    "admin.get_peers",
    "admin.get_roles",
    "admin.get_peer",
    "admin.get_realm",
    "admin.get_tenant",
    "admin.get_role"
  ]

  def start_link(
        name: name,
        db: db,
        realm: realm,
        realms: realms,
        platform_password: platform_password
      ) do
    GenServer.start_link(__MODULE__, {db, realm, realms, platform_password}, name: name)
  end

  def init({db, realm, realms, platform_password}) do
    {:ok,
     %{
       db: db,
       realm: realm,
       platform_password: platform_password,
       proxy: Realms.start_realm(realms, realm),
       regs: []
     }, {:continue, :ok}}
  end

  def handle_continue(:ok, %{db: db, realm: realm, regs: regs} = state) do
    regs = register_procedures(db, realm, regs)
    {:noreply, %{state | regs: regs}}
  end

  def register_procedures(db, realm, regs) do
    Logger.info("Registering admin procedures: #{inspect(@procedures)}")

    Enum.reduce(@procedures, regs, fn proc, acc ->
      val = {RealmSession.get_id(), {self(), Node.self()}}
      RealmSession.register(db, realm, proc, val)
      [{proc, val} | acc]
    end)
  end

  def handle_info(
        {req_id, %Invocation{details: %{"procedure" => "admin.get_tenants"}}, {pid, node}},
        %{proxy: proxy} = state
      ) do
    tenants = Tenant.get_all()
    send({proxy, node}, {%Result{request_id: req_id, arg_list: Tenant.to_map(tenants)}, pid})
    {:noreply, state}
  end

  def handle_info(
        {req_id,
         %Invocation{details: %{"procedure" => "admin.get_roles"}, arg_kw: %{"tenant" => tenant}},
         {pid, node}},
        %{proxy: proxy} = state
      ) do
    tenant = Tenant.get(uri: tenant)
    roles = Role.get_for_tenant(tenant: tenant)
    send({proxy, node}, {%Result{request_id: req_id, arg_list: Role.to_map(roles)}, pid})
    {:noreply, state}
  end

  def handle_info(
        {req_id,
         %Invocation{details: %{"procedure" => "admin.get_peers"}, arg_kw: %{"realm" => realm}},
         {pid, node}},
        %{proxy: proxy} = state
      ) do
    realm = Realm.get(uri: realm)
    peers = Peer.get_for_realm(realm: realm)
    send({proxy, node}, {%Result{request_id: req_id, arg_list: Peer.to_map(peers)}, pid})
    {:noreply, state}
  end

  def handle_info(
        {req_id,
         %Invocation{
           details: %{"procedure" => "admin.create_tenant"},
           arg_kw: %{"uri" => uri, "admin_authid" => authid, "admin_password" => password}
         }, {pid, node}},
        %{proxy: proxy, realm: realm, platform_password: platform_password} = state
      ) do
    with %Tenant{} = tenant <- Tenant.create(uri: uri),
         %Realm{} = realm <- Realm.get(uri: realm) do
      create_tenant_admin(tenant, realm, authid, password)
      create_default_realm(tenant, platform_password)
      send({proxy, node}, {%Result{request_id: req_id, arg_kw: Tenant.to_map(tenant)}, pid})
    else
      er ->
        Logger.info("Error #{inspect(er)}")
        send({proxy, node}, {%Error{request_id: req_id, error: "error"}, pid})
    end

    {:noreply, state}
  end

  def handle_info(
        {req_id,
         %Invocation{
           details: %{"procedure" => "admin.create_realm"},
           arg_kw: %{"uri" => uri, "tenant" => tenant_uri}
         }, {pid, node}},
        %{proxy: proxy, platform_password: platform_password} = state
      ) do
    with %Tenant{} = tenant <- Tenant.get(uri: tenant_uri),
         %Realm{} = realm <- Realm.create(uri: tenant_uri <> "." <> uri, tenant: tenant) do
      create_platform_peer(tenant, realm, platform_password)
      publish_realm(realm)
      send({proxy, node}, {%Result{request_id: req_id, arg_kw: Realm.to_map(realm)}, pid})
    else
      er ->
        Logger.info("Error #{inspect(er)}")
        send({proxy, node}, {%Error{request_id: req_id, error: "error"}, pid})
    end

    {:noreply, state}
  end

  def handle_info(
        {req_id,
         %Invocation{
           details: %{"procedure" => "admin.create_peer"},
           arg_kw: %{
             "authid" => authid,
             "password" => password,
             "realm" => realm,
             "roles" => roles,
             "tenant" => tenant
           }
         }, {pid, node}},
        %{proxy: proxy} = state
      ) do
    with %Tenant{} = tenant <- Tenant.get(uri: tenant),
         %Realm{} = realm <- Realm.get(uri: realm),
         %Peer{} = peer <-
           Peer.create(authid: authid, password: password, realm: realm, tenant: tenant) do
      roles =
        Enum.map(roles, fn name ->
          Role.get(name: name, tenant: tenant)
        end)

      Peer.add_roles(peer, roles)
      send({proxy, node}, {%Result{request_id: req_id, arg_kw: Peer.to_map(peer)}, pid})
    else
      er ->
        Logger.info("Error #{inspect(er)}")
        send({proxy, node}, {%Error{request_id: req_id, error: "error"}, pid})
    end

    {:noreply, state}
  end

  def handle_info(
        {req_id,
         %Invocation{
           details: %{"procedure" => "admin.create_role"},
           arg_kw: %{
             "name" => name,
             "tenant" => tenant,
             "permissions" => perms
           }
         }, {pid, node}},
        %{proxy: proxy} = state
      ) do
    with %Tenant{} = tenant <- Tenant.get(uri: tenant),
         %Role{} = role <- Role.create(name: name, tenant: tenant, super: false) do
      Enum.each(perms, fn p ->
        Permission.create(type: get_in(p, ["type"]), rule: get_in(p, ["rule"]), role: role)
      end)

      send({proxy, node}, {%Result{request_id: req_id, arg_kw: Role.to_map(role)}, pid})
    else
      er ->
        Logger.info("Error #{inspect(er)}")
        send({proxy, node}, {%Error{request_id: req_id, error: "error"}, pid})
    end

    {:noreply, state}
  end

  def handle_info(
        {req_id,
         %Invocation{details: %{"procedure" => "admin.get_tenant"}, arg_kw: %{"uri" => uri}},
         {pid, node}},
        %{proxy: proxy} = state
      ) do
    tenant = Tenant.get(uri: uri)
    send({proxy, node}, {%Result{request_id: req_id, arg_kw: Tenant.to_map(tenant)}, pid})
    {:noreply, state}
  end

  def handle_info(
        {req_id,
         %Invocation{details: %{"procedure" => "admin.get_realm"}, arg_kw: %{"uri" => uri}},
         {pid, node}},
        %{proxy: proxy} = state
      ) do
    realm = Realm.get(uri: uri)
    send({proxy, node}, {%Result{request_id: req_id, arg_kw: Realm.to_map(realm)}, pid})
    {:noreply, state}
  end

  def handle_info(
        {req_id,
         %Invocation{
           details: %{"procedure" => "admin.get_peer"},
           arg_kw: %{"authid" => authid, "realm" => realm}
         }, {pid, node}},
        %{proxy: proxy} = state
      ) do
    realm = Realm.get(uri: realm)
    peer = Peer.get(authid: authid, realm: realm)
    send({proxy, node}, {%Result{request_id: req_id, arg_kw: Peer.to_map(peer)}, pid})
    {:noreply, state}
  end

  def handle_info(
        {req_id,
         %Invocation{
           details: %{"procedure" => "admin.get_role"},
           arg_kw: %{"id" => id}
         }, {pid, node}},
        %{proxy: proxy} = state
      ) do
    role = Role.get(id: id)
    send({proxy, node}, {%Result{request_id: req_id, arg_kw: Role.to_map(role)}, pid})
    {:noreply, state}
  end

  defp publish_realm(realm) do
    Client.publish(
      AdminClient,
      %Publish{
        topic: "admin.realm.created",
        arg_kw: Realm.to_map(realm)
      }
    )
  end

  defp create_tenant_admin(%Tenant{uri: uri} = tenant, realm, authid, password) do
    %Peer{} =
      peer =
      Peer.create(authid: authid <> "@" <> uri, password: password, realm: realm, tenant: tenant)

    %Role{} = admin_role = Role.create(name: "admin", tenant: tenant, super: false)
    Permission.create(type: "*", rule: "admin.", role: admin_role)
    Peer.add_roles(peer, [admin_role])
  end

  defp create_default_realm(%Tenant{uri: uri} = tenant, platform_password) do
    %Realm{} = realm = Realm.create(uri: uri <> ".default", tenant: tenant)
    create_platform_peer(tenant, realm, platform_password)
    publish_realm(realm)
  end

  defp create_platform_peer(%Tenant{} = tenant, %Realm{uri: uri} = realm, password) do
    %Peer{} =
      peer =
      Peer.create(authid: "platform@" <> uri, password: password, realm: realm, tenant: tenant)

    %Role{permissions: permissions} =
      role = Role.create(name: "platform", tenant: tenant, super: true)

    case permissions do
      [] ->
        Permission.create(type: "*", rule: "*", role: role)

      _ ->
        :noop
    end

    Peer.add_roles(peer, [role])
  end
end
