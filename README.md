# Router

The Router application is the core WAMP Router for the EIOTA platform, it relies heavily on the [WAMPexRouter](https://gitlab.com/entropealabs/wampex_router) package.

WAMPexRouter allows for a modular authentication and authorization system. This router implements the [wampcra](https://wamp-proto.org/_static/gen/wamp_latest.html#wampcra) specification for authentication and a robust RBAC system that relies on topics and procedire namespaces for authorization.

In addition to the auth modules, we implement an `Admin` realm for administrative tasks within the Platform.

There are several system variables utilized for bootstrapping a brand new system.

```bash
export SEED_TENANT=org.entropealabs
export SEED_REALM=admin
export SEED_AUTHID=admin
export SEED_PASSWORD=test1234
export PLATFORM_PASSWORD=test1234
export AUTH_DATABASE_NAME=auth
export AUTH_DATABASE_HOSTNAME=localhost
export AUTH_DATABASE_USERNAME=root
```

`SEED_TENANT` is our default tenant, this will generally be related to the company running the platform.

`SEED_REALM` is the name of the `admin` realm we create. This can be whatever you want, but `admin` is a sane default.

`SEED_AUTHID` is the default user and is always considered a super user, this is the id you would use to login to an admin interface and begin configuring the platform

`SEED_PASSWORD` is the password/secret used with the id above.

`AUTH_DATABASE_NAME` is the database name used to back the auth modules

`AUTH_DATABASE_HOSTNAME` is the hostname for our db

`AUTH_DATABASE_USERNAME` is the username for authenticating to our DB.


