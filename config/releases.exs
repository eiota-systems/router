import Config

config :router, Router.Auth.Repo, hostname: System.get_env("AUTH_DATABASE_HOSTNAME")
