use Mix.Config

config :logger,
  level: :info,
  compile_time_purge_matching: [
    [level_lower_than: :info]
  ]

config :router, Router.Auth.Repo,
  database: "auth",
  hostname: "localhost",
  port: 5432,
  username: "postgres",
  password: "password"

config :router,
  topologies: []
