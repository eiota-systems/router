defmodule Router.MixProject do
  use Mix.Project

  def project do
    [
      app: :router,
      version: "0.1.0",
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      releases: releases(),
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {Router, []}
    ]
  end

  defp releases do
    [
      app: [
        include_executables_for: [:unix],
        applications: [router: :permanent, runtime_tools: :permanent],
        cookie: "router_cookie"
      ]
    ]
  end

  defp deps do
    [
      {:ecto_sql, "~> 3.7"},
      {:eep,
       git: "https://github.com/virtan/eep.git", tag: "8f6e5e3ade0606390d928830db61350a5451dda8"},
      {:postgrex, ">= 0.0.0"},
      {:recon, "~> 2.5"},
      {:wampex_client, "~> 0.1"},
      {:wampex_router, "~> 0.1"},
      {:poison, "~> 5.0"}
    ]
  end
end
